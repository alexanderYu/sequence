package com.alex.yu;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by alexander on 09.05.17.
 */
public class SequenceImplTest {
    private Sequence sequence = new SequenceImpl();

    @Before
    public void fromArrayTest(){
        String data[] = {"a", "b"};
        sequence.fromArray(data);

    }

    @Test
    public void headTest(){
        Object o = sequence.head();
        assertEquals("a", o);
    }

    @Test
    public void tailTest(){
        Sequence s = sequence.tail();
        assertEquals("b", s.head());
        assertNull(s.tail());
    }

    @Test
    public void seqTest(){
        Sequence s = sequence.seq("h", sequence);
        assertEquals("h", s.head());
        assertEquals("a", s.tail().head());
        assertEquals("a", sequence.head());
    }
}
