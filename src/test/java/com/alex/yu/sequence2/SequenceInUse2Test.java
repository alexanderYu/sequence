package com.alex.yu.sequence2;

import com.alex.yu.Sequence;
import com.alex.yu.SequenceImpl;
import com.alex.yu.SequenceInUse;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNull;
import static junit.framework.TestCase.assertEquals;


/**
 * Created by alexander on 09.05.17.
 */
public class SequenceInUse2Test {
    private Sequence sequence = new SequenceImpl();

    @Before
    public void beforeTest(){
        String data[] = {"a", "b", "c", "d", "e"};
        sequence.fromArray(data);

    }

    @Test
    public void rotateAndGetByIndexTest(){
        SequenceInUse2 sequenceInUse = new SequenceInUse2();
        Object res =  sequenceInUse.rotateAndGetByIndex(sequence, -2, 1);
        assertEquals("d", res);
        res =  sequenceInUse.rotateAndGetByIndex(sequence, 2, 2);
        assertEquals("a", res);
        res =  sequenceInUse.rotateAndGetByIndex(sequence, 7, 2);
        assertEquals("a", res);
        res =  sequenceInUse.rotateAndGetByIndex(sequence, 2, 7);
        assertNull(res);
    }
}
