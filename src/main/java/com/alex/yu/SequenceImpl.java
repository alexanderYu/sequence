package com.alex.yu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SequenceImpl implements Sequence {

    interface AddToList{
        void apply(List list,Sequence sequence);
    }

    private Object[] elementData;

    public SequenceImpl() {
        this.elementData = new String[0];
    }

    private SequenceImpl(Object[] elementData){
        this.elementData = elementData;
    }

    // implement Sequence interface

    @Override
    public Object head() {
        if(elementData == null || elementData.length == 0)
            return null;
        return elementData[0];
    }

    @Override
    public Sequence tail() {
        if(elementData == null || elementData.length == 0)
            return null;
        Object[] newElementData = Arrays.copyOfRange(elementData, 1, elementData.length);
        if(newElementData.length == 0)
            return null;
        return new SequenceImpl(newElementData);
    }

    @Override
    public Sequence seq(Object aHead, Sequence aSequence) {
        List<Object> listSeq = new ArrayList<>();
        listSeq.add(aHead);

//        if(aSequence != null)
        new AddToList() {
            @Override
            public void apply(List list, Sequence sequence) {
                if (sequence != null) {
                    list.add(sequence.head());
                    this.apply(list,sequence.tail());
                }
            }
        }.apply(listSeq, aSequence);

        return new SequenceImpl(listSeq.toArray(new Object[listSeq.size()]));
    }

    @Override
    public Sequence fromArray(String[] aArray) {
        if(aArray == null || aArray.length == 0)
            return null;
        elementData = new Object[aArray.length];
        int i = 0;
        for (String s : aArray){
            elementData[i++] = s;
        }
        return new SequenceImpl(elementData);
    }

    @Override
    public String toString() {
        return "SequenceImpl{" +
                "elementData=" + Arrays.toString(elementData) +
                '}';
    }
}
