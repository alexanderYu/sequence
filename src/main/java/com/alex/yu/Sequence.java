package com.alex.yu;

/**
 * Created by alexander on 06.05.17.
 */
public interface Sequence {

    // first element in the sequence
    Object head();

    // rest of the sequence, if null -> the head was the last element
    Sequence tail();

    // creates new sequence
    Sequence seq(Object aHead, Sequence aSequence);

    // helper method (used only once to init test-seq)
    Sequence fromArray(String[] aArray);

}

    //////////////////////////////////////////////////////////////////////////////////////////////

