package com.alex.yu;


import com.alex.yu.Sequence;

/**
 * Created by alexander on 06.05.17.
 */
public class SequenceInUse {

    interface Rotate{
        Sequence rotate(Sequence aSeq, int aRotation);
    }

    public Object getLast(Sequence sequence){
        if(sequence.tail() != null)
            return getLast(sequence.tail());
        return sequence.head();
    }

    public Sequence reverse(Sequence sequenceOut, Sequence sequenceIn){
        sequenceOut = sequenceIn.seq(sequenceIn.head(), sequenceOut);
        if(sequenceIn.tail() != null)
            return reverse(sequenceOut, sequenceIn.tail());
        return sequenceOut;
    }

    public Sequence getWithoutLast(Sequence sequenceOut, Sequence sequenceIn){
        if(sequenceIn.tail() != null) {
            sequenceOut = sequenceIn.seq(sequenceIn.head(), sequenceOut);
            return getWithoutLast(sequenceOut, sequenceIn.tail());
        }
        return reverse(null, sequenceOut);
    }

    public Integer getTrueRotation(Sequence aSeq, int aRotation){
        Integer amountElements = getCountElements(0, aSeq);
        Integer rotation = aRotation % amountElements;
        return (rotation < 0) ? (rotation + amountElements) : rotation;
    }

    private Integer getCountElements(Integer count, Sequence aSeq) {
        if (aSeq.tail() != null){
            return getCountElements(++count, aSeq.tail());
        }
        return ++count;
    }

    public Object rotateAndGetByIndex(Sequence aSeq, int aRotation, int aIndex) {
        // implement this function
        if (aSeq == null)
            return null;
        aRotation = getTrueRotation(aSeq, aRotation);

        Sequence sequence = new Rotate() {
            @Override
            public Sequence rotate(Sequence aSeq, int aRotation) {
                if (aRotation > 0) {
                    Object o = getLast(aSeq);
                    Sequence res = aSeq.seq(o, getWithoutLast(null, aSeq));
                    return rotate(res, --aRotation);
                }
                return aSeq;
            }

        }.rotate(aSeq, aRotation);

        return getObjectByIndex(sequence, aIndex);
    }

    private Object getObjectByIndex(Sequence sequence, int aIndex) {
        if(aIndex < 0 || sequence == null)
            return null;
        else if(aIndex == 0)
            return sequence.head();
        return getObjectByIndex(sequence.tail(), --aIndex);
    }
}
