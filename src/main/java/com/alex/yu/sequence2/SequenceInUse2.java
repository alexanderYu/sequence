package com.alex.yu.sequence2;


import com.alex.yu.Sequence;

/**
 * Created by alexander on 09.05.17.
 */
public class SequenceInUse2 {

    private Tail getTail(){
        return Sequence::tail;
    }

    private Object getLast(Sequence sequence) {
        if (sequence.tail() != null)
            return getLast(sequence.tail());
        return sequence.head();
    }

    private Sequence reverse(Sequence sequenceIn) {
        Sequence sequenceOut = null;
        do {
            sequenceOut = sequenceIn.seq(sequenceIn.head(), sequenceOut);
            sequenceIn = getTail().get(sequenceIn);
        }while (sequenceIn != null);
        return sequenceOut;
    }

    private Sequence getWithoutLast(Sequence sequenceIn) {
        if(sequenceIn == null)
            return null;
        Sequence sequenceOut = null;
        Sequence sequence = getTail().get(sequenceIn);
        while (sequence != null){
            sequenceOut = sequenceIn.seq(sequenceIn.head(), sequenceOut);
            sequenceIn = sequence;
            sequence = getTail().get(sequence);
        }
        return reverse(sequenceOut);
    }

    private Integer getCountElements(Sequence inSequence) {
        Sequence sequence = inSequence;
        int count = 0;
        while (sequence != null){
            sequence = getTail().get(sequence);
            ++count;
        }
        return count;
    }

    public Object rotateAndGetByIndex(Sequence aSeq, int aRotation, int aIndex) {
        // implement this function
        if (aSeq == null)
            return null;

        RotationCounter rotationCounter = (sequence, aRotation1) -> {
            Integer amountElements = getCountElements(sequence);
            Integer rotation = aRotation1 % amountElements;
            return (rotation < 0) ? (rotation + amountElements) : rotation;
        };

        OneStepRotator oneStepRotator = aSeq1 -> {
            Object o = getLast(aSeq1);
            return aSeq1.seq(o, getWithoutLast(aSeq1));
        };

        Rotator rotator = (oneStepRotator1, rotationCounter1) -> {
            int n = rotationCounter1.countUp(aSeq, aRotation);
            Sequence sequence = aSeq;
            while (n-- > 0){
                sequence = oneStepRotator1.rotateUpOnce(sequence);
            }
            return sequence;
        };
        Sequence sequence = rotator.rotateNTimes(oneStepRotator, rotationCounter);
        return getObjectByIndex(sequence, aIndex);
    }

    private Object getObjectByIndex(Sequence sequence, int aIndex) {
        if (aIndex < 0 || sequence == null)
            return null;
        while (aIndex != 0){
            sequence = getTail().get(sequence);
            if(sequence == null)
                return null;
            --aIndex;
        }
        return sequence.head();
    }
}