package com.alex.yu.sequence2;

import com.alex.yu.Sequence;

/**
 * Created by alexander on 09.05.17.
 */
@FunctionalInterface
public interface Rotator {
    Sequence rotateNTimes(OneStepRotator oneStepRotator, RotationCounter rotationCounter);
}
